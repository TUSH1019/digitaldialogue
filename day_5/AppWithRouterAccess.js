import Header from "./components/Header";
import Home from "./components/home/Home";
import { Security, SecureRoute, LoginCallback } from '@okta/okta-react';
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';
import React from 'react'
import {Box} from "@material-ui/core"
import DetailView from "./components/post/DetailView";
import {BrowserRouter, Routes, Route, useNavigate} from "react-router-dom";
import CreateView from "./components/post/CreateView";
import UpdateView from "./components/post/UpdateView";
import { oktaAuthConfig, oktaSignInConfig } from './config';
//import AppWithRouterAccess from './AppWithRouterAccess';


import Login from './components/account/Login';

import config from './config';

const oktaAuth = new OktaAuth(oktaAuth.Config);
function AppWithRouterAccess ()  {

    const history = useNavigate();

    const customAuthHandler = () => {
        history.push('/login');
      };
    
      const restoreOriginalUri = async (_oktaAuth, originalUri) => {
        history.replace(toRelativeUrl(originalUri || '', window.location.origin));
      };
  return (
    <Security
        oktaAuth={oktaAuth}
        onAuthRequired={customAuthHandler}
        restoreOriginalUri={restoreOriginalUri}
      >
    <SecureRoute path='/' component={Header}/>
      <Box style={{marginTop: 64 }}>
        <Routes>
          <Route exact path="/" component={Home} />
          <Route path="/login" render={() => <Login config={oktaSignInConfig} />} />
        <Route path="/login/callback" component={LoginCallback} />

          <Route exact path="/details/:id" componen={DetailView} />
          <Route exact path="/create" component={CreateView } />
          <Route exact path="/update/:id" component={UpdateView } />
        </Routes>
    </Box>
    </Security>
  );
}

export default AppWithRouterAccess;
