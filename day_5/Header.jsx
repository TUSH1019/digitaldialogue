import { AppBar, Toolbar, Typography,makeStyles, Button } from "@material-ui/core"
import {Link, useNavigate} from 'react-router-dom'
import { useOktaAuth } from "@okta/okta-react";


const useStyles = makeStyles({

  component: {
    background: '#FFFFFF',
    color: 'black'
},
container: {
    justifyContent: 'center',
    '&  >*': {
        padding: 20
    }
},
Link:{
        color: 'black',
        textDecoration: 'none'
    }


})
const Header = () => {
  const classes = useStyles()
  const history = useNavigate()
  const { oktaAuth, authState } = useOktaAuth();

  if (authState && authState.isPending) return null;

  const login = async () => history('/login');

  const logout = async () => oktaAuth.signOut();

  const button = authState.isAuthenticated ? 
        <Button onClick={logout} style={{
            background: 'unset',
            border: 'none',
            fontSize: 17,
            textTransform: 'uppercase',
            fontFamily: 'Roboto',
            cursor: 'pointer',
            opacity: 0.8
        }}>Logout</Button> :
        <Button onClick={login}>Login</Button>;

  
  return (
    
   <AppBar className={classes.component}>
     <Toolbar className={classes.container}>
       <Link className={classes.Link} to='/'><Typography>Home</Typography></Link>
       <Typography>About</Typography>
       <Typography>Contact</Typography>
       <Typography>{button}</Typography>
     </Toolbar>
   </AppBar>
  )
}

export default Header