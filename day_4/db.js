import mongoose from 'mongoose';



const Connection = async () => {
    try{
        const URL = 'mongodb://user:mongo@blogweb-shard-00-00.jv2qp.mongodb.net:27017,blogweb-shard-00-01.jv2qp.mongodb.net:27017,blogweb-shard-00-02.jv2qp.mongodb.net:27017/BLOG?ssl=true&replicaSet=atlas-2h82xm-shard-0&authSource=admin&retryWrites=true&w=majority';
        

        await mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true});
        console.log("Database successfully connected");
    } catch(error){
        console.log("Error while connecting to MongoDB", error);
    }
}

export default Connection;