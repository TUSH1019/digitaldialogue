var slider = document.getElementById("myRange");
				var pitch = document.getElementById("myPitchs");		
				//var txtInput = document.querySelector('#txtInput');
                var txtInput = document.querySelector("#testing2");
				var voiceList = document.querySelector('#voiceList');
				var btnSpeak = document.querySelector('#btnSpeak');
				var btnPause = document.querySelector('#btnPause');
				var btnResume = document.querySelector('#btnResume');
				var btnStop = document.querySelector('#btnStop');
				var synth = window.speechSynthesis;
				var voices = [];

                //out.print(txtInput);
				PopulateVoices();
				if(speechSynthesis !== undefined){
					speechSynthesis.onvoiceschanged = PopulateVoices;
				}
				btnSpeak.addEventListener('click', ()=> {
					var toSpeak = new SpeechSynthesisUtterance(txtInput.innerHTML);
					var selectedVoiceName = voiceList.selectedOptions[0].getAttribute('data-name');
					voices.forEach((voice)=>{
						if(voice.name === selectedVoiceName){
							toSpeak.voice = voice;
						}
					});
					toSpeak.pitch = pitch.value;
					toSpeak.rate = slider.value;
					synth.speak(toSpeak);
					btnPause.addEventListener('click',()=>{
						synth.pause(toSpeak);
					});
					btnResume.addEventListener('click',()=>{
						synth.resume(toSpeak);
					});
					btnStop.addEventListener('click',()=>{
						synth.cancel(toSpeak);
					});
				});
				function PopulateVoices(){
					voices = synth.getVoices();
					var selectedIndex = voiceList.selectedIndex < 0 ? 0 : voiceList.selectedIndex;
					voiceList.innerHTML = '';
					voices.forEach((voice)=>{
						var listItem = document.createElement('option');
						listItem.textContent = voice.name;
						listItem.setAttribute('data-lang', voice.lang);
						listItem.setAttribute('data-name', voice.name);
						voiceList.appendChild(listItem);
					});
					voiceList.selectedIndex = selectedIndex;
				}