
import Example from './useSpeechSynthesis';
import React from 'react'

function App() {
  return (
    <div className="App">
      <Example/>
    </div>
  );
}

export default App;
