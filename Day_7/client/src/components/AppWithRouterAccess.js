import Header from "./Header";
import Home from "./home/Home";
import { Security, SecureRoute, LoginCallback } from '@okta/okta-react';
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';
import { oktaAuthConfig, oktaSignInConfig } from '../config';
import React from 'react'
import {Box} from "@material-ui/core"
import DetailView from "./post/DetailView";
import { BrowserRouter, Switch, Route ,useHistory } from 'react-router-dom';
import CreateView from "./post/CreateView";
import UpdateView from "./post/UpdateView";
import Login from "./account/Login";
import About from "./about/About";
import Contact from "./contact/Contact";

const oktaAuth = new OktaAuth(oktaAuthConfig);

function AppWithRouterAccess() {
    const history = useHistory();

    const restoreOriginalUri = async (_oktaAuth, originalUri) => {
        history.replace(toRelativeUrl(originalUri, window.location.origin));
    };

    const customAuthHandler = () => {
        history.push('/login');
    };
    

  return (
      
    
    <Security
    oktaAuth={oktaAuth}
    onAuthRequired={customAuthHandler}
    restoreOriginalUri={restoreOriginalUri}
>

    <SecureRoute path='/' component={Header}/>
    <Box style={{marginTop: 64 }}>
      <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/login' render={() => <Login config={oktaSignInConfig} />} />
            <Route path='/login/callback' component={LoginCallback} />
            <Route exact path='/details/:id' component={DetailView} />
            <Route exact path='/create' component={CreateView} />
            <Route exact path='/update/:id' component={UpdateView} />
            <Route exact path='/about' component={About} />
            <Route exact path='/contact' component={Contact} />
            
          </Switch>
    </Box>
    </Security>
  );
}

export default AppWithRouterAccess;