import React from 'react'
import { Button,TableBody,TableCell, TableRow,Table,TableHead  } from '@material-ui/core';
import { categories } from '../../constants/data';
import { makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyle = makeStyles({
    create:{
        margin: 20,
        background: "#074449",
        color : "WHITE",
        width: "86%",


    },

    table: {
        border: '1px solid rgba(224, 224, 224, 1)',
        background: "#F1F1E6",
        color: "white"

        
    },
    write: {
        margin: 20,
        width: '85%',
        background: '',
        color: 'WHITE',
        textDecoration: 'none'
    },
    link: {
        textDecoration: 'none',
        color: 'BLACK'
    }
})

const Categories = ({match}) => {
    const classes = useStyle();
  return (
    <>
        <Link to = 'create' className={classes.link}><Button variant = "contained" className={classes.create}>Create Blog</Button></Link>

        <Table className={classes.table}>
        <TableHead>
            <TableRow>
                
                    <TableCell>
                        <Link to={'/'} className={classes.link}>
                            All Categories
                            </Link>
                            </TableCell> 
                
            </TableRow>
        </TableHead>
        <TableBody>
                    {
                        categories.map(category => (
                            <TableRow>
                                <TableCell>
                                    <Link to = {`/?category=${category}`} className={classes.link}>
                                        {category}
                                    </Link>
                                </TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
        </Table>
    </>
    
  )
}

export default Categories