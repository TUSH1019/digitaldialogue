import React from 'react'
import { Box,FormControl,InputBase } from '@material-ui/core'
import { makeStyles, Typography,TextareaAutosize} from '@material-ui/core';
import {AddCircle} from '@material-ui/icons';
import { Button } from '@material-ui/core';
import { useState,useEffect } from 'react';
import { getPost,updatePost,uploadFile } from '../service/api';
import {useHistory} from "react-router-dom"

const useStyle = makeStyles((theme) => ({
    container: {
        margin: '50px 100px',
        [theme.breakpoints.down('md')]: {
            margin: 0
        },
    },
    image: {
        width: '70%',
        height: '80vh',
        objectFit: 'cover'
    },
    form:{
        display:"flex",
        flexDirection:"row",
        marginTop: 10
    },
    title: {
        marginTop: 10,
        display: 'flex',
        flexDirection: 'row'
    },
    textfield: {
        flex: 1,
        margin: '0 30px',
        fontSize: 25
    },
    textarea: {
        width: '100%',
        border: 'none',
        marginTop: 50,
        fontSize: 18,
        '&:focus-visible': {
            outline: 'none'
        }}
}))
    
const initialValues = {
    title: '',
    description: '',
    picture: '',
    username: 'DigitalDialogue',
    categories: '',
    createdDate: new Date()
}
    
    const UpdateView = ({match}) => {
        const classes = useStyle(initialValues);
        const history = useHistory()
    
    const [post,setPost] = useState(initialValues)
    const [file, setFile] = useState('');
    const [image, setImage] = useState('');
    const url = post.picture? post.picture : 'https://images.unsplash.com/photo-1543128639-4cb7e6eeef1b?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bGFwdG9wJTIwc2V0dXB8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80'
    
    
    
    useEffect(() => {
            const getImage = async () => { 
                console.log(file)
                if(file) {
                    const data = new FormData();
                    data.append("name", file.name);
                    data.append("file", file);
                    
                    const image = await uploadFile(data);
                    post.picture = image.data;
                    setImage(image.data);
                }
            }
            getImage();
            //post.categories = location.search?.split('=')[1] || 'All'
            //post.username = account;
        }, [file]) 
    

    useEffect(() => {
        const fetchData = async() =>{
            let data = await getPost(match.params.id)
            setPost(data)
        }
        fetchData();
    },[])

    const handleChange = (e) => {
        setPost({...post,[e.target.name]: e.target.value})
    }

    const updateBlog = async() => {
        await updatePost(match.params.id,post)
        history.push(`/details/${match.params.id}`)
    }

    return (
      <Box className={classes.container}>
    <img src={url} alt="banner" className={classes.image}/> 
    <FormControl className={classes.form}>

            <label htmlFor = "fileinput" >
            <AddCircle fontSize='large' color='action'/>
            &nbsp;
            </label>
                
                <input
            type="file"
            id = "fileinput"

            style = {{display : 'none'}}
            onChange={(e) => setFile(e.target.files[0])}
            
    />

        <InputBase placeholder=' Title' name="title" onChange={(e) => handleChange(e)} value={post.title} className={classes.textField}/>
        <InputBase placeholder='Choose' name="categories" onChange={(e) => handleChange(e)} value={post.categories} className={classes.textField}/>
        <Button  onClick={() => updateBlog()} variant="contained" color ="primary">update</Button>
    </FormControl>
    <TextareaAutosize
    minRows={5}
    placeholder="Tell your story"
    className={classes.textarea}
    value={post.description}
    onChange={(e) => handleChange(e)}
    name = "description"


    />
    </Box>
  )
}

export default UpdateView