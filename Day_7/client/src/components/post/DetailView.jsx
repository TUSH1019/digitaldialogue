import {Box, makeStyles, Typography} from '@material-ui/core';
import {Edit,Delete,KeyboardVoice} from '@material-ui/icons'
import { Link} from 'react-router-dom';
import { useEffect,useState } from 'react';
import { getPost,deletePost } from '../service/api';
import {useHistory} from "react-router-dom"
import Example from '../../UseSpeechSynthesis';

import "../../PopUpSpeak.css";
import Modal from '../../ModalComponents/Modal';
import Comments from '../comments/Comments';

const useStyle = makeStyles((theme) => ({
    container: {
        margin: '50px 100px',
        [theme.breakpoints.down('md')]: {
            margin: 0
        },
    },
    image: {
        width: '100%',
        height: '50vh',
        objectFit: 'cover'
    },
    icons: {
        float: 'right'
    },
    icon: {
        margin: 5,
        padding: 5,
        border: '1px solid #878787',
        borderRadius: 10
    },
    heading: {
        fontSize: 38,
        fontWeight: 600,
        textAlign: 'center',
        margin: '50px 0 10px 0'
    },
    subheading:{
        color: '#878787',
        display: 'flex',
        margin: '20px 0',
        [theme.breakpoints.down('sm')]: {
            display: 'block'
        },
    },
    author: {
        color: '#878787',
        display: 'flex',
        margin: '20px 0',
        [theme.breakpoints.down('sm')]: {
            display: 'block'
        },
    },
    link: {
        textDecoration: 'none',
        color: 'inherit'
    }
}))



const DetailView = ({ match }) => {
    const classes = useStyle();
    const url = "https://images.unsplash.com/photo-1543128639-4cb7e6eeef1b?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bGFwdG9wJTIwc2V0dXB8ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80"
    const [post, setPost] = useState({});
    const history = useHistory()
    const [modalOpen, setModalOpen] = useState(false);
    
    useEffect(() => {
        const fetchData = async () => {
            let data = await getPost(match.params.id);
            console.log(data);
            setPost(data);

        }
        fetchData();
    }, [])    

    const deleteBlog = async () =>{
        await deletePost(post._id)
        history.push("/")

    }

    return (
        <Box className={classes.container}>
            <img src={post.picture || url} alt="banner" className='classes.image'/> 
            

            <Box className={classes.icons} >
                <Link to = {`/update/${post._id}`}><Edit className={classes.icon} color="primary"/></Link>
                <Link><Delete onClick ={() => deleteBlog() }className={classes.icon} color="error"/></Link>
                <KeyboardVoice className={classes.icon} onClick={() => { setModalOpen(true); }}/>
                {modalOpen && <Modal setOpenModal={setModalOpen}/> && <Example desc = {post.description}/>}

            </Box>
            <Typography className={classes.heading}>{post.title}</Typography>
            <Box className={classes.subheading}>
                <Box>
                <Link to = {`/?username=${post.username}`} className= {classes.link}>
                <Typography>Author: &nbsp; <span style ={{fontWeight: 600}}>{post.username}</span></Typography>
                </Link>
                {"\n"}
                <Link to = {`/?category=${post.categories}`} className= {classes.link}>
                <Typography>Category: &nbsp; <span style ={{fontWeight: 600}}>{post.categories}</span></Typography>
                </Link>
                </Box>
                
                <Typography style={{marginLeft: 'auto'}}>{new Date(post.createdDate).toDateString()}</Typography>
                
            </Box>

            <Typography>{post.description}</Typography>
            <Comments post = {post} />

        </Box>
        

    )
}
export default DetailView;