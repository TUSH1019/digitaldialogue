import { AppBar, Toolbar, Typography, makeStyles, Button } from '@material-ui/core'; 
import { Link } from 'react-router-dom';

import { useHistory } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';


const useStyles = makeStyles({

  component: {
    background: '#EAC89A',
    color: 'white'
},
container: {
    justifyContent: 'center',
    '&  >*': {
        padding: 20
    }
},
Link:{
        color: 'black',
        textDecoration: 'none'
    }


})
const Header = () => {
  const classes = useStyles()

  const history = useHistory();
    const { oktaAuth, authState } = useOktaAuth();

    if (authState && authState.isPending) return null;
    

    

    const login = async () => history.push('/login');
    
    const logout = async () => oktaAuth.signOut();

    const button = authState.isAuthenticated ? 
        <Button onClick={logout} style={{
            background: 'unset',
            border: 'none',
            fontSize: 15,
            cursor: 'pointer',
            opacity: 0.8,
            color:"black"
        }}>Logout</Button> :
        <Button onClick={login}>Login</Button>;


  return (
    
   <AppBar className={classes.component}>
     
     <Toolbar className={classes.container}>

       <Link className={classes.Link} to='/'><Typography>HOME</Typography></Link>
       <Link  className={classes.Link} to='/about'>ABOUT</Link>
                <Link  className={classes.Link} to='/contact'>CONTACT</Link>
       
       <Typography>{button}</Typography>
     </Toolbar>
   </AppBar>
  )
}

export default Header