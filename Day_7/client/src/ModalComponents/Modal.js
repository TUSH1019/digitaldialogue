import React from "react";
import "./Modal.css";
import Example from "../UseSpeechSynthesis";

function Modal({ setOpenModal }) {
  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
            
          <button className="X" onClick={() => { setOpenModal(false); }}>
                X
          </button>
          <Example/>
        </div>
      </div>
    </div>
  );
}

export default Modal;
