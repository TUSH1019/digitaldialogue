import React, { useState } from "react";
import "./PopUpSpeak.css";
import Modal from "./ModalComponents/Modal";


function Speak() {
  const [modalOpen, setModalOpen] = useState(false);

  return (
     
    <div className="SpeakText">
      
      <button className="openModalBtn" onClick={() => { setModalOpen(true); }}>
        Speak for me!
      </button>

      {modalOpen && <Modal setOpenModal={setModalOpen}/>} 
    </div>
  );
}

export default Speak;