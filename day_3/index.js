import express from "express";

//connections
import Connection from './database/db.js';

const app = express();
const PORT = 8000;
app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));

Connection();